var input,filter, ul, li, a, i, txtValue;
var filterInput = document.getElementById('filterInput');
var filterValue = document.getElementById("filterInput").value.toUpperCase();
var ul = document.getElementById("names");
var li = ul.querySelectorAll("li.collection-item");
filterInput.addEventListener('keyup', filterNames)
function filterNames() {
    // get value of input
    var filterValue = document.getElementById('filterInput').value.toUpperCase();

    // Get names ul
    var li = ul.querySelectorAll("li.collection-item");

    for (var i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName('a')[0];
        // txtValue = a.textContent || a.innerHTML;
        if (a.innerHTML.toUpperCase().indexOf(filterValue) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = 'none';
        }
    }

}