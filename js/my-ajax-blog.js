"use strict";

var ajaxRequest = $.ajax("data/my-ajax-post.json");

ajaxRequest.done(function (data) {
    console.log(data)

    for (var i = 0; i < data.cities.length; i++){
        var city =`
            <div class="col-4">
                <div class="card">
                    <div id="card" class= "card-header">
                        ${data.cities[i].title}
                    </div>
                    
                    <img src="${data.cities[i].img}">
                    <P>${data.cities[i].content}</P>
                    <br>
                    <P>Known for:</P>
                    <P>${data.cities[i].categories}</P>
                    <input type="submit" class="btn btn-outline-info" value="For More Information Click Here...">
                    
                </div>
            </div>
        `;
       $("#destinations").append(city);
    }
});


